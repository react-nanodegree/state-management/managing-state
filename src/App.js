import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const numOfOperands = 3;
const summandMax = 100;
const sumDeviationMax = 5;

class Equation extends Component {
  render(){
    let sumText = this.props.data.numbers.reduce((acc, cur) => `${acc} + ${cur}` );
    return (
      <div className="equation">
      	<p className="text">{`${sumText} = ${this.props.data.proposedAnswer}`}</p>
	  </div>
    )
  }
}

class ScoreBoard extends Component {
	render(){
		return (
			<p className="text">
              Your Score: {this.props.numCorrect}/{this.props.numQuestions}
            </p>
		)
	}
}

class App extends Component {
  state = {
	...this.createNumbers(), 
	numQuestions: 0, 
	numCorrect: 0,
    userInput: '',
    isCorrect: null
  }
  createNumbers(){
	let stateObj = {};
    let sum = (acc, cur) => acc + cur;
    stateObj.numbers = [];
  	for(let i = 1; i <= numOfOperands; i++){
      stateObj.numbers.push(Math.floor(Math.random() * summandMax));
    }
    stateObj.realAnswer = stateObj.numbers.reduce(sum);
    stateObj.proposedAnswer = Math.floor(Math.random() * sumDeviationMax) + stateObj.realAnswer;
    
    return stateObj;
  }
  checkAnswer(event){
	let isCorrect = false;
	let numQuestions = this.state.numQuestions;
	let numCorrect = this.state.numCorrect;

	if(event.target.innerText === 'True'){
	  if(this.state.proposedAnswer === this.state.realAnswer){
		isCorrect = true;
      	numCorrect++;
      }
    } else {
	  if(this.state.proposedAnswer !== this.state.realAnswer){
		isCorrect = true;
      	numCorrect++;
      }
	}
	
	
	numQuestions++;
	this.setState({...this.createNumbers(), isCorrect, numCorrect, numQuestions});
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ReactND - Coding Practice</h1>
        </header>
        <div className="game">
          <h2>Mental Math</h2>
          <Equation data={this.state} />
          <button onClick={(e) => this.checkAnswer(e)}>True</button>
          <button onClick={(e) => this.checkAnswer(e)}>False</button>
          <ScoreBoard numCorrect={this.state.numCorrect} numQuestions={this.state.numQuestions} />
        </div>
      </div>
    );
  }
}

export default App;
